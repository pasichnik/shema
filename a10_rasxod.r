#�10. ������ ���������������� �������� (��� ������ �� ��������� � �����)
a10_rasxod <- function(df,y1, y_out, Y_BASE, OB_COND, y_ent,  OB0_NONINF_MODE, 
                       OB0_NONINF_SUMM, noninf1_5, OB0_NONINF_UNIT){
  
  df$ob0_noninf <- 0
  df$ob0_noninf_enter <- 0
  df$ob0_noning_enter_ind <- 0
  
  
  ob0_y_enter <- ifelse(OB_COND == "1",y1,y_ent)
  ob0_noninf_summ <- ifelse(OB0_NONINF_MODE ==1,noninf1_5,
                            ifelse(OB0_NONINF_MODE == 2,OB0_NONINF_SUMM,
                                   OB0_NONINF_UNIT*(df$m0_work[Y_BASE-year_start+1]+df$m1_work[Y_BASE-year_start+1])))
  i <- y1-year_start+1
  while(i<y_out-year_start+1){
    df$ob0_noninf_enter[i] <- ifelse(i==ob0_y_enter-year_start+1,ob0_noninf_summ,0)
    df$ob0_noning_enter_ind[i] <- df$ob0_noninf_enter[i]*df$k_consum_pr_base[i]
    df$ob0_noninf[i] <- ifelse(df$ob0_noninf_enter[i]>0,df$ob0_noning_enter_ind[i],df$ob0_noninf[i-1]*df$k_consum_pr_ch[i])
     i <- i+1
  }
  return(df)
}

noninf1_5 <- sum(gg[i,names(gg)[grepl("OB0_NONINF\\d",names(gg))]])
df <- a10_rasxod(df,y1, y_out, gg$Y_BASE[i], gg$OB_COND[i], y_ent, gg$OB0_NONINF_MODE[i], 
                    gg$OB0_NONINF_SUMM[i], noninf1_5, gg$OB0_NONINF_UNIT[i])