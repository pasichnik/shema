# �5. ������ ���������� ��������� � ����������� ��� �������
a5_ost_spoim <- function(df,y1, y_out, ob0_k, OB_COND, OB0_D_ENTER, OB0_COST, OB0_AM, OB0_T_AM,  
                         OB0_AM_CONST, OB0_INV_MODE, OB0_INV_SUMM, OB0_INV_N, OB0_INV_V,OB0_N,OB0_V,Y_BASE){
  
  df$ob0_cost_enter <- 0
  df$ob0_cost_enter_ind <- 0
  df$ob0_cost_beg <- 0
  df$ob0_cost_end <- 0
  df$ob0_am <- 0
  df$ob0_taxprop_base <- 0
  
  #comment
  
  if(OB_COND == "1"){
    ob0_cost <- OB0_COST
  }else{
    if(OB0_INV_MODE =="1"){
      ob0_cost <- OB0_INV_SUMM
    }else{
      ob0_cost <- OB0_INV_N*OB0_N+OB0_INV_V*OB0_V
    }
  }
  
  i <- y1-year_start+1
  while(i<=y_out-year_start+1){
    if((OB_COND =="1" & i==y1-year_start+1) | (i==y_ent-year_start+1 & OB_COND =="2")){
      df$ob0_cost_enter[i] <- ob0_cost 
    } else{
      df$ob0_cost_enter[i] <- 0
    }
    df$ob0_cost_enter_ind[i] <- df$ob0_cost_enter[i]*df$k_constr_pr_base[i-1]
    i <- i+1
  }
  
  ob0_am_dis <-ifelse(OB_COND =="1", OB0_AM, df$ob0_cost_enter_ind[y_ent-year_start+1]/OB0_T_AM)

  ob0_cost_end0 <- ifelse(OB_COND =="1" | y1 == y_ent, ob0_cost ,0)

  
  i <- y1-year_start+1
  while (i<=y_out-year_start+1) {
    if((OB_COND == "1" & i=="1") | (i==y_ent-year_start+1 & OB_COND =="2") | df$ob0_cost_enter[i]>0){
      df$ob0_cost_beg[i] <- df$ob0_cost_enter_ind[i]
    }else{
      df$ob0_cost_beg[i] <- df$ob0_cost_end[i-1]
    }
    if(OB0_AM_CONST=="1"){
      df$ob0_am[i] <- ob0_am_dis
      df$ob0_cost_end[i] <- df$ob0_cost_beg[i]
    }else{
      df$ob0_am[i] <- min(df$ob0_cost_beg[i],ob0_am_dis*df$ob0_k[i])
      df$ob0_cost_end[i] <- df$ob0_cost_beg[i]-df$ob0_am[i]
    }
    df$ob0_taxprop_base[i] <- (df$ob0_cost_beg[i]+df$ob0_cost_end[i])*df$ob0_k[i]/2
    i <- i+1
  }
  return(df)
}




