# �4. ������ ������ �� �����������
a4_deb_per <- function(df,y1, y_out, k_sec_res, l_comp, k_rdf, e, k_burn, v_sec_res, v_comp, v_rdf, v_e) {
  
  
  df$m_sec_res <-  df$M0*df$P0_SEC_RES + df$M1*df$P1_SEC_RES
  df$m_comp <-  df$M0*df$P0_COMP + df$M1*df$P1_COMP
  df$m_rdf <-  df$M0*df$P0_RDF + df$M1*df$P1_RDF
  df$m_e <-  df$M0*df$P0_E + df$M1*df$P1_E
  
  df$m_sec_res_net <-  df$m_sec_res * k_sec_res
  df$m_comp_net <-  df$m_comp * (1 - l_comp)
  df$m_rdf_net <-  df$m_rdf * k_rdf
  df$m_e_net <-  df$m_e * k_burn
  
  df$m_sec_res_rest <-  df$m_sec_res - df$m_sec_res_net
  df$m_rdf_rest <-  df$m_rdf - df$m_rdf_net
  df$m_e_rest <-  df$m_e - df$m_e_net
  
  df$m_rest <-  df$m_sec_res_rest + df$m_rdf_rest + df$m_e_rest
  
  df$e_net <-  df$m_e * e
  
  df$inc_sec_res <-  df$m_sec_res_net * v_sec_res * df$k_consum_pr_base/1000
  df$inc_comp <-  df$m_comp_net * v_comp * df$k_consum_pr_base/1000
  df$inc_rdf <-  df$m_rdf_net * v_rdf * df$k_consum_pr_base/1000
  df$inc_e <-  df$e_net * v_e * df$k_elect_pr_base/1000
  
  df$inc_res <-  df$inc_sec_res + df$inc_comp + df$inc_rdf + df$inc_e
  
  return(df)
  

}





