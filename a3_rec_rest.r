# �3. ������ ��������, �����������, �����
a3_rec_res <- function(objkt,y1, y_out, y_ent, y_ent1,OB_COND, OB_TYPE, OB0_V, OB0_N, OB1_N, OB1_V, F_N) {
  objkt$m_unw_summ<- 0
  objkt$n_end <- 0
  objkt$v_end <- 0
  objkt$n_beg <- 0
  objkt$v_beg <- 0
  objkt$n_delta <- 0
  objkt$v_delta <- 0
  
  objktv_delta <- 0
  objkt$m0_work <- 0
  objkt$m1_work <- 0
  objkt$m0_unw <- 0
  objkt$m1_unw <- 0
  objkt$n_beg_rest <- 0
  objkt$v_beg_rest <- 0
  
  objkt$n_end[y1-1-year_start+1] <- ifelse(OB_COND == "1" | y1==y_ent,OB0_N,0) # n_end0
  objkt$v_end[y1-1-year_start+1] <- ifelse(OB_COND == "1" | y1==y_ent,OB0_V,0) # v_end0
  

  i <- y1-year_start+1
  while(i<=y_out-year_start+1){
    if(i==(y_ent-year_start+1) & OB_COND!="1"){
      objkt$n_beg[i] <- OB0_N
      objkt$v_beg[i] <- OB0_V
    }else{
      objkt$n_beg[i] <- objkt$n_end[i-1]
      objkt$v_beg[i] <- objkt$v_end[i-1]
    }
    
    objkt$n_delta[i] <- ifelse(i==y_ent1-year_start+1,OB1_N,0)
    objkt$v_delta[i] <- ifelse(i==y_ent1-year_start+1,OB1_V,0)
  
    objkt$n_end[i] <- objkt$n_beg[i]+objkt$n_delta[i]
    
    if(OB_TYPE == 1){
      objkt$m1_work[i] <- ifelse(F_N == "1", min(objkt$M1[i],objkt$v_beg[i],objkt$n_beg[i]*objkt$ob0_k[i]),
                                     min(objkt$M1[i],objkt$v_beg[i]))
    }else{
      objkt$m1_work[i] <- min(objkt$M1[i],objkt$n_beg[i]*objkt$ob0_k[i])
    }
    
    
    objkt$m1_unw[i] <- max(objkt$M1[i]-objkt$m1_work[i],0)
    objkt$n_beg_rest[i] <- objkt$n_beg[i]*objkt$ob0_k[i]-objkt$m1_work[i]
    objkt$v_beg_rest[i] <- objkt$v_beg[i]-objkt$m1_work[i]
    
    if(OB_TYPE ==1){
      objkt$m0_work[i] <- ifelse(F_N=="1",min(objkt$M0[i],objkt$v_beg_rest[i],objkt$n_beg_rest[i]),
                                  min(objkt$M0[i],objkt$v_beg_rest[i]))
    }else{
      objkt$m0_work[i] <- min(objkt$M0[i],objkt$n_beg_rest[i])
    }
    objkt$m0_unw[i] <- max(objkt$M0[i]-objkt$m0_work[i],0)
    objkt$v_end[i] <- objkt$v_beg_rest[i]-objkt$m0_work[i]+objkt$v_delta[i]
    objkt$m_unw_summ[i] <- objkt$m1_unw[i]+objkt$m0_unw[i]+ifelse((i-1)==0,0,objkt$m_unw_summ[i-1])

    
    i <- i+1
  } 

  return(objkt)
    
}
