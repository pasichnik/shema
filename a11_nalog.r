# �11. ������ ������ �� ���������

a11_nalog <- function(df,y1, y_out, OB0_TAXPROP_MODE, OB0_TAXPROP_SUMM, OB0_TAXPROP_CONST, OB0_COST, r_tax_prop){
  
  
  df$taxprop <- 0
  df$taxprop_base <- 0
  
  if(OB0_TAXPROP_CONST){
    i <- y1-year_start+1
    while(i<=y_out-year_start+1){
      df$taxprop[i] <- OB0_TAXPROP_SUMM 
      i <- i+1
    }
    return(df)
  }else{
    i <- y1-year_start+1
    while(i<=y_out-year_start+1){
      df$taxprop_base[i] <- df$ob0_taxprop_base[i]+df$ob1_taxprop_base[i]
      i <- i+1
    }
  }
  if(OB0_TAXPROP_MODE =='1'){
    i <- y1-year_start+1
    while(i<=y_out-year_start+1){
      df$taxprop[i] <- df$taxprop_base[i]*r_tax_prop
      i <- i+1
    }
  }else{
    ob0_taxprop_unit <- OB0_TAXPROP_SUMM/OB0_COST
    i <- y1-year_start+1
    while(i<=y_out-year_start+1){
      df$taxprop[i] <- df$ob0_taxprop_base[i]*ob0_taxprop_unit
      i <- i+1
    }   
  }
  return(df)
}
