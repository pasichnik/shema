# �8.2. ������ ���������� ��� ������� - ����� ���������� (�� ������� ������� 1)
a82_inv <- function(df,y1, y_out, y_ent1, OB1_INV_MODE, OB1_INV_SUMM, OB1_INV_N, OB1_INV_V, 
                    OB1_N, OB1_V, OB1_INV_K_BUDG, OB1_T_CR,OB1_T_AM){
  df$ob1_inv_summ <- 0
  df$ob1_inv_summ_ind <- 0
  df$ob1_debt_beg <- 0
  df$ob1_debt_end <- 0
  
  
  if(OB1_INV_MODE ==1){
    ob1_inv_summ_dis <- OB1_INV_SUMM*(1-OB1_INV_K_BUDG)
  }else{
    ob1_inv_summ_dis <- (OB1_INV_N*OB1_N+OB1_INV_V*OB1_V)*(1-OB1_INV_K_BUDG)
  }
  i <- y1-1-year_start+1
  while(i<=y_out-year_start+1){
    if(i==y_ent1-1-year_start+1){
      df$ob1_inv_summ[i] <- ob1_inv_summ_dis
      df$ob1_inv_summ_ind[i] <- df$ob1_inv_summ[i]*df$k_constr_pr_base[i]
    }else{
      df$ob1_inv_summ[i] <- 0
      df$ob1_inv_summ_ind[i] <- 0
    }
    i <- i+1
  }
  ob1_paym_dis <- df$ob1_inv_summ_ind[y_ent1-1-year_start+1]/OB1_T_AM
  df$ob1_debt_beg <- 0
  df$ob1_paym <- 0
  df$ob1_debt_end <- 0
  df$ob1_debt_end[y1-1-year_start+1] <-  df$ob1_inv_summ_ind[y1-1-year_start+1]
  
  i <- y1-year_start+1
  while(i<=y_out-year_start+1){
    df$ob1_debt_beg[i] <- ifelse(df$ob1_inv_summ[i]>0,df$ob1_inv_summ_ind[i],df$ob1_debt_end[i-1])
    df$ob1_paym[i] <- min(df$ob1_debt_beg[i],df$ob1_paym_dis[i])
    df$ob1_debt_end[i] <- df$ob1_debt_beg[i]-df$ob1_paym[i]
    i <- i+1
  }
  return(df)  
}
