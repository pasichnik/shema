library(xlsx)
library(readxl)
library(stringr)
options(stringsAsFactors=FALSE)

# ��� ������� ��� 2014
year_start <- 2014 # ����� � 2014 -1

infs <- read_excel("penz/infs.xlsx")



infs <- infs[infs$���������� == "�������� �������",-(2:7)]
infs <- infs[!is.na(infs$id),]
infs[is.na(infs)] <-0 

gg <- read_excel("penz/complete_new_google_form.xlsx", 
                 col_types = rep("numeric",262), skip = 2) # perm 262 ,data 667

gg1 <- read_excel("penz/complete_new_google_form.xlsx", n_max = 1)
gg1[1,1] <- "id"
names(gg) <- gg1[1,]
gg <- gg[,  names(gg)[names(gg)!="NA"]   ]
gg <- gg[!is.na(gg$id),]
gg1 <- NULL

gg[is.na(gg)] <- 0

gg$OB0_D_ENTER <- as.Date(gg$OB0_D_ENTER,origin ="1899-12-30")
gg$OB0_D_OUT <- as.Date(gg$OB0_D_OUT,origin ="1899-12-30")
gg$OB1_D_ENTER <- as.Date(gg$OB1_D_ENTER,origin ="1899-12-30")

# ���������
cons <- read_excel("penz/����� ���������.xlsx")
cons <- cons[,c("label","test_value")]
cons <- as.data.frame(t(cons),stringsAsFactors = FALSE)
names(cons) <- cons["label",]
cons <- cons[-1,]
for (i in 1:length(cons)) {
  cons[,i] <- as.numeric(cons[,i])
}

y_cur <- as.numeric(format(Sys.time(), "%Y"))

orig <- data.frame(row.names = 2014:(2013+length(unlist(cons[grepl("k_constr_pr_ch_",names(cons))]))),
  k_constr_pr_ch=unlist(cons[grepl("k_constr_pr_ch_",names(cons))]),
  k_consum_pr_ch=unlist(cons[grepl("k_consum_pr_ch_",names(cons))]),
  k_elect_pr_ch=unlist(cons[grepl("k_elect_pr_ch_",names(cons))]),
  k_fuel_pr_ch=unlist(cons[grepl("k_fuel_pr_ch_",names(cons))]),
  k_gas_pr_ch=unlist(cons[grepl("k_gas_pr_ch_",names(cons))]))
temp <- orig[max(row.names(orig)),]
max_y_out <- max(format(gg$OB0_D_OUT,"%Y")) 
j <- as.numeric(max((rownames(orig))))
while(j<max_y_out){
  j <- j+1
  orig[as.character(j),] <- temp
}
write.xlsx2(orig,"tarif.xlsx",sheetName = "penz")
i <- 1
#
# ====================================================================================================
#
all_tax <- data.frame(row.names = year_start:(year_start+length(orig$k_constr_pr_ch)-1))

for(i in 1:length(gg$id)){
  
  y1 <- start_year(gg$Y1_MODE[i],cons$Y1,gg$OB_COND[i],y_cur,gg$OB0_D_ENTER[i])
  y_ent  <- as.numeric(format(gg$OB0_D_ENTER[i], "%Y"))
  y_ent1 <- as.numeric(format(gg$OB1_D_ENTER[i], "%Y"))
  y_out <- as.numeric(format(gg$OB0_D_OUT[i], "%Y"))
  objkt <- a0_base_ind(orig,gg$Y_BASE[i],y_out)
  objkt <- coef_use_ob(objkt,y1,y_ent,y_out,gg$OB0_D_ENTER[i],gg$OB0_D_OUT[i],gg$OB_COND[i])
  
  objkt$M0 <- 0
  objkt[names(unlist(infs[infs$id == gg$id[i],-1])),'M0'] <- unlist(infs[infs$id == gg$id[i],-1])

  objkt$M1 <- 0
  temp <- t(gg[i,names(gg)[str_sub(names(gg),1,3) == "M1_"]])
  objkt[(y1-year_start+1):(y1-year_start+length(temp)),'M1']<- temp

  
  objkt$ob1_am <- 0
  objkt$ob1_cost_beg <- 0
  objkt$ob1_cost_end <- 0
  objkt$ob1_cost_enter <- 0
  objkt$ob1_cost_enter_ind <- 0
  objkt$ob1_debt_beg <- 0
  objkt$ob1_debt_end <- 0
  objkt$ob1_inv_summ <- 0
  objkt$ob1_inv_summ_ind <- 0
  objkt$ob1_k <- 0
  objkt$ob1_paym <- 0
  objkt$ob1_paym_debt <- 0
  objkt$ob1_paym_pers <- 0
  objkt$ob1_taxprop_base <- 0
  objkt$m_unw_summ <- 0
  objkt$n_end <- 0
  objkt$v_end <- 0
  objkt$n_beg <- 0
  objkt$v_beg <- 0
  objkt$n_delta <- 0
  objkt$v_delta <- 0
  objkt$m0_work <- 0
  objkt$m1_work <- 0
  objkt$m0_unw <- 0
  objkt$m1_unw <- 0
  objkt$n_beg_rest <- 0
  objkt$v_beg_rest <- 0
  objkt$P0_SEC_RES <- 0
  objkt$P0_COMP <- 0
  objkt$P1_SEC_RES <- 0
  objkt$P1_COMP <- 0
  objkt$P0_RDF <- 0
  objkt$P1_RDF <- 0
  objkt$P0_E <- 0
  objkt$P1_E <- 0
  objkt$ob0_cost_enter <- 0
  objkt$ob0_cost_enter_ind <- 0
  objkt$ob0_cost_beg <- 0
  objkt$ob0_cost_end <- 0
  objkt$ob0_am <- 0
  objkt$ob0_taxprop_base <- 0
  objkt$ob0_debt_end <- 0
  objkt$ob1_inv_summ_ind_prc <- 0
  objkt$ob1_paym_debr <- 0
  objkt$ob0_debt_beg <- 0
  objkt$ob0_paym_debt <- 0
  objkt$ob0_paym <- 0
  objkt$ob0_paym_dis <- 0
  objkt$ob0_inv_summ <- 0
  objkt$ob0_inv_summ_ind <- 0
  objkt$ob0_inv_summ_ind_prc <- 0
  objkt$ob0_paym_pers <- 0
  objkt$ob0_opex <- 0
  objkt$ob0_opex_enter <- 0
  objkt$ob0_opex_enter_ind <- 0
  objkt$k_iie_base <- 0
  objkt$ob0_noninf <- 0
  objkt$ob0_noninf_enter <- 0
  objkt$ob0_noning_enter_ind <- 0
  objkt$taxprop <- 0
  objkt$taxprop_base <- 0
  objkt$ehp <- 0
  objkt$P0EHP1 <- 0
  objkt$P0EHP2 <- 0
  objkt$P0EHP3 <- 0
  objkt$P0EHP4 <- 0
  objkt$P1EHP1 <- 0
  objkt$P1EHP2 <- 0
  objkt$P1EHP3 <- 0
  objkt$P1EHP4 <- 0
  objkt$p_ehp1 <- 0
  objkt$p_ehp2 <- 0
  objkt$p_ehp3 <- 0
  objkt$p_ehp4 <- 0
  objkt$p_ehp5 <- 0
  objkt$m_ehp1 <- 0
  objkt$m_ehp2 <- 0
  objkt$m_ehp3 <- 0
  objkt$m_ehp4 <- 0
  objkt$m_ehp5 <- 0
  objkt$p0_ehp5 <- 0
  objkt$p1_ehp5 <- 0
  objkt$c_ehp1 <- 0
  objkt$c_ehp2 <- 0
  objkt$c_ehp3 <- 0
  objkt$c_ehp4 <- 0
  objkt$c_ehp5 <- 0
  objkt$enr <- 0
  objkt$TAX_FORCED <- 0
  objkt$RDM_CORR <- 0
  objkt$b_pr <- 0
  objkt$n_pr <- 0
  objkt$taxprof <- 0
  objkt$c_cur <- 0
  objkt$rdm <- 0
  objkt$tax <- 0
  objkt$m_sec_res <- 0
  objkt$m_comp <- 0
  objkt$m_rdf <- 0
  objkt$m_e <- 0
  objkt$m_sec_res_net <- 0
  objkt$m_comp_net <- 0
  objkt$m_rdf_net <- 0
  objkt$m_e_net <- 0
  objkt$m_sec_res_rest <- 0
  objkt$m_rdf_rest <- 0
  objkt$m_e_rest <- 0
  objkt$m_rest <- 0
  objkt$e_net <- 0
  objkt$inc_sec_res <- 0
  objkt$inc_comp <- 0
  objkt$inc_rdf <- 0
  objkt$inc_e <- 0
  objkt$inc_res <- 0
  

  objkt<- a3_rec_res(objkt,y1,y_out,y_ent,y_ent1,gg$OB_COND[i],gg$OB_TYPE[i],gg$OB0_V[i],gg$OB0_N[i],
                gg$OB1_N[i],gg$OB1_V[i],gg$F_N[i])
  
  temp <- t(gg[i,names(gg)[grepl("P0_SEC_RES_",names(gg))]])
  objkt[(y1-year_start+1):(y1-year_start+length(temp)),'P0_SEC_RES'] <- temp
  temp <- t(gg[i,names(gg)[grepl("P0_COMP_",names(gg))]])
  objkt[(y1-year_start+1):(y1-year_start+length(temp)),'P0_COMP'] <- temp
  temp <- t(gg[i,names(gg)[grepl("P1_SEC_RES_",names(gg))]])
  objkt[(y1-year_start+1):(y1-year_start+length(temp)),'P1_SEC_RES'] <- temp
  temp <- t(gg[i,names(gg)[grepl("P1_COMP_",names(gg))]])
  objkt[(y1-year_start+1):(y1-year_start+length(temp)),'P1_COMP'] <- temp
  temp <- t(gg[i,names(gg)[grepl("P0_RDF_",names(gg))]])
  objkt[(y1-year_start+1):(y1-year_start+length(temp)),'P0_RDF'] <- temp
  temp <- t(gg[i,names(gg)[grepl("P1_RDF_",names(gg))]])
  objkt[(y1-year_start+1):(y1-year_start+length(temp)),'P1_RDF'] <- temp
  temp <- t(gg[i,names(gg)[grepl("P0_E_",names(gg))]])
  objkt[(y1-year_start+1):(y1-year_start+length(temp)),'P0_E'] <- temp
  temp <- t(gg[i,names(gg)[grepl("P1_E_",names(gg))]])
  objkt[(y1-year_start+1):(y1-year_start+length(temp)),'P1_E'] <- temp
  
  if(gg$OB_TYPE[i] !=1){
    objkt <- a4_deb_per(objkt, y1, y_out, cons$k_sec_res, cons$l_comp, cons$k_rdf, cons$e,
                        cons$k_burn, cons$v_sec_res, cons$v_comp, cons$v_rdf, cons$v_e)
  }
  
  objkt <- a5_ost_spoim(objkt,y1, y_out, ob0_k,gg$OB_COND[i],gg$OB0_D_ENTER[i],gg$OB0_COST[i],gg$OB0_AM[i],gg$OB0_T_AM[i], 
        gg$OB0_AM_CONST[i], gg$OB0_INV_MODE[i], gg$OB0_INV_SUMM[i], gg$OB0_INV_N[i], gg$OB0_INV_V[i],gg$OB0_N[i],gg$OB0_V[i],gg$Y_BASE[i])
  
  

  
  objkt <- a6_ost_price(objkt,y1, y_out, y_ent1, gg$OB1_D_ENTER[i], gg$OB1_T_AM[i],  gg$OB1_INV_MODE[i], 
                     gg$OB1_INV_SUMM[i], gg$OB1_INV_N[i], gg$OB1_INV_V[i], gg$OB1_N[i], gg$OB1_V[i])
  
  
  if(gg$METH[i]=="1"){

    objkt <- a71_invest(objkt,y1, y_out, y_ent, gg$OB_COND[i], gg$OB0_INV_MODE[i], gg$OB0_INV_SUMM[i],gg$OB0_INV_N[i], 
                     gg$OB0_INV_V[i], gg$OB0_N[i], gg$OB0_V[i], gg$OB0_INV_K_BUDG[i], gg$OB0_R_CR[i], gg$OB0_T_CR[i])
    
    objkt <- a81_inv(objkt,y1, y_out, y_ent1, gg$OB1_INV_MODE[i], gg$OB1_INV_SUMM[i], gg$OB1_INV_N[i], gg$OB1_INV_V[i], 
                     gg$OB1_N[i], gg$OB1_V[i], gg$OB1_INV_K_BUDG[i], gg$OB1_R_CR[i], gg$OB1_T_CR[i])
    }else{
    
      objkt <- a72_invest(objkt,y1, y_out, y_ent,gg$OB_COND[i], gg$OB0_INV_MODE[i], gg$OB0_INV_SUMM[i], gg$OB0_INV_N[i], 
                         gg$OB0_INV_V[i], gg$OB0_N[i], gg$OB0_V[i], gg$OB0_INV_K_BUDG[i], gg$OB0_T_CR[i],gg$OB0_T_AM[i])
      
      objkt <- a82_inv(objkt,y1, y_out, y_ent1, gg$OB1_INV_MODE[i], gg$OB1_INV_SUMM[i], gg$OB1_INV_N[i], gg$OB1_INV_V[i], 
                       gg$OB1_N[i], gg$OB1_V[i], gg$OB1_INV_K_BUDG[i], gg$OB1_T_CR[i], gg$OB1_T_AM[i])  
   }
    
  
  opex1_10 <- sum(gg[i,names(gg)[grepl("OB0_OPEX\\d",names(gg))]])
  objkt <- a9_rash(objkt,y1, y_out, gg$Y_BASE[i], gg$OB_COND[i], y_ent, cons$ie, gg$OB0_OPEX_MODE[i], 
                   gg$OB0_OPEX_SUMM[i], opex1_10, gg$OB0_OPEX_UNIT[i])
  
  
  noninf1_5 <- sum(gg[i,names(gg)[grepl("OB0_NONINF\\d",names(gg))]])
  objkt <- a10_rasxod(objkt,y1, y_out, gg$Y_BASE[i], gg$OB_COND[i], y_ent, gg$OB0_NONINF_MODE[i], 
                      gg$OB0_NONINF_SUMM[i], noninf1_5, gg$OB0_NONINF_UNIT[i])
  
  objkt <- a11_nalog(objkt,y1, y_out, gg$OB0_TAXPROP_MODE[i], gg$OB0_TAXPROP_SUMM[i], gg$OB0_TAXPROP_CONST[i], gg$OB0_COST[i], cons$r_tax_prop)
  
  objkt$ehp <- 0
  
  if(gg$OB_TYPE[i] =="1"){
    
    
    temp <- t(gg[i,names(gg)[grepl("P0EHP1_\\d",names(gg))]])
    if(length(temp>0)){
      objkt[(y1-year_start+1):(y1-year_start+length(temp)),'P0EHP1'] <- temp
      temp <- t(gg[i,names(gg)[grepl("P0EHP2_\\d",names(gg))]])
      objkt[(y1-year_start+1):(y1-year_start+length(temp)),'P0EHP2'] <- temp
      temp <- t(gg[i,names(gg)[grepl("P0EHP3_\\d",names(gg))]])
      objkt[(y1-year_start+1):(y1-year_start+length(temp)),'P0EHP3'] <- temp
      temp <- t(gg[i,names(gg)[grepl("P0EHP4_\\d",names(gg))]])
      objkt[(y1-year_start+1):(y1-year_start+length(temp)),'P0EHP4'] <- temp
    }else{
      objkt$P0EHP1 <- 0
      objkt$P0EHP2 <- 0
      objkt$P0EHP3 <- 0
      objkt$P0EHP4 <- 0
    }
    
    temp <- t(cons[1,names(cons)[grepl("p_ehp1_\\d",names(cons))]])
    objkt[(y1-year_start+1):(y1-year_start+length(temp)),'p_ehp1'] <- temp
    temp <- t(cons[1,names(cons)[grepl("p_ehp2_\\d",names(cons))]])
    objkt[(y1-year_start+1):(y1-year_start+length(temp)),'p_ehp2'] <- temp
    temp <- t(cons[1,names(cons)[grepl("p_ehp3_\\d",names(cons))]])
    objkt[(y1-year_start+1):(y1-year_start+length(temp)),'p_ehp3'] <- temp
    temp <- t(cons[1,names(cons)[grepl("p_ehp4_\\d",names(cons))]])
    objkt[(y1-year_start+1):(y1-year_start+length(temp)),'p_ehp4'] <- temp
    temp <- t(cons[1,names(cons)[grepl("p_ehp5_\\d",names(cons))]])
    objkt[(y1-year_start+1):(y1-year_start+length(temp)),'p_ehp5'] <- temp
    
    objkt <- a12_pnvos(objkt,y1, y_out,  gg$OB0_EHP_MODE[i],gg$OB0_EHP_SUMM[i],gg$Y_BASE[i])
  }
  objkt <- a13_energo(objkt, y1, y_out, gg$Y_BASE[i],gg$OB0_ENR_MODE[i], gg$OB0_ENR_SUMM[i], gg$OB0_ENR_ELECT[i],
                   gg$OB0_ENR_FUEL[i], gg$OB0_ENR_GAS[i], gg$OB0_ENR_UNIT[i])
  
  temp <- t(gg[i,names(gg)[grepl("TAX_FORCED_\\d",names(gg))]])
  objkt[(y1-year_start+1):(y1-year_start+length(temp)),'TAX_FORCED'] <- temp
  
  temp <- t(gg[i,names(gg)[grepl("RDM_CORR_\\d",names(gg))]])
  objkt[(y1-year_start+1):(y1-year_start+length(temp)),'RDM_CORR'] <- temp
  
  
  if(gg$METH[i] == "1"){
    
    objkt <- a14_prib(objkt,y1, y_out, cons$r_b_prof)
    objkt <- a15_prib(objkt)
    objkt <- a17_1_nalog(objkt,cons$r_tax_prof)
    objkt <- a18_rashod(objkt)
    if(gg$OB_TYPE[i] == '1'){
      objkt <- a19_1_nvv(objkt)
    }else{
      objkt <- a19_3_nvv(objkt)
    }
  }else{
    objkt <- a16_prib(objkt,cons$r_marg)
    objkt <- a17_2_nalog(objkt,cons$r_tax_prof)
    if(gg$OB_TYPE == 1){
      objkt <- a19_2_nvv(objkt)
    }else{
      objkt <- a19_4_nvv(objkt)
    }
  }
  
  objkt <- a20_tarif(objkt)
  
  
  
  print(gg$id[i])
  print(c(objkt$tax))
  df_tax <- data.frame(obj=objkt$tax)
  names(df_tax) <- gg$id[i]
  all_tax <- cbind(all_tax,df_tax)
  write.xlsx2(t(objkt),sheetName = paste0(gg$id[i]),file = "tarif.xlsx",append = TRUE)
}

write.xlsx2(gg,sheetName = "gg","in.xlsx")
write.xlsx2(infs,sheetName = "M0","in.xlsx",append = TRUE)

