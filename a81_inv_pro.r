#�8.1. ������ ���������� ��� ������� - ����� ���������� (�� ������� ������� 1)
a81_inv <- function(df,y1, y_out, y_ent1, OB1_INV_MODE, OB1_INV_SUMM, OB1_INV_N, OB1_INV_V, OB1_N, 
                    OB1_V, OB1_INV_K_BUDG, OB1_R_CR, OB1_T_CR){
  
  df$ob1_inv_summ <- 0
  df$ob1_inv_summ_ind <- 0
  df$ob1_inv_summ_ind_prc <- 0
  df$ob1_paym_debt <- 0
  df$ob1_paym_debr <- 0
  df$ob1_debt_beg <- 0
  df$ob1_paym <- 0
  df$ob1_paym_pers <- 0
  df$ob1_debt_end <- 0
  
  ob1_inv_summ_dis <- ifelse(OB1_INV_MODE == "1",OB1_INV_SUMM*(1-OB1_INV_K_BUDG),
                             (OB1_INV_N*OB1_N+OB1_INV_V*OB1_V)*(1-OB1_INV_K_BUDG))
  
  ob1_inv_summ_dis <- ifelse(is.na(ob1_inv_summ_dis),0,ob1_inv_summ_dis)
  
  i <- y1-1-year_start+1
  while (i<=y_out-year_start+1) {
    df$ob1_inv_summ[i] <- ifelse(i==y_ent1-1-year_start+1,ob1_inv_summ_dis,0)
    df$ob1_inv_summ_ind[i] <- df$ob1_inv_summ[i]*df$k_constr_pr_base[i]
    df$ob1_inv_summ_ind_prc[i] <- df$ob1_inv_summ_ind[i]*(1+OB1_R_CR)
    i <- i+1
  }
  
  ob1_paym_dis <- df$ob1_inv_summ_ind_prc[y_ent1-1-year_start+1]*(OB1_R_CR/(1-(1+OB1_R_CR)^(-OB1_T_CR)))
  
  
  
  df$ob1_debt_end[y1-1-year_start+1] <- df$ob1_inv_summ_ind_prc[y1-1-year_start+1]
  i <- y1-year_start+1
  while (i<=y_out-year_start+1) {
	df$ob1_debt_beg[i] <- df$ob1_debt_end[i-1]
	df$ob1_paym_pers[i] <- df$ob1_debt_beg[i]*OB1_R_CR
	df$ob1_paym[i] <- min(df$ob1_debt_beg[i]+df$ob1_paym_pers[i],ob1_paym_dis)
	df$ob1_paym_debt[i] <- df$ob1_paym[i]-df$ob1_paym_pers[i]
    df$ob1_debt_end[i] <- ifelse(df$ob1_inv_summ[i]>0,df$ob1_inv_summ_ind_prc[i],df$ob1_debt_beg[i]-df$ob1_paym_debt[i])
    i <- i+1 
  }
  df$ob1_paym[is.na(df$ob1_paym)] <- 0
  df$ob1_paym_debt[is.na(df$ob1_paym_debt)] <- 0
  df$ob1_paym_pers[is.na(df$ob1_paym_pers)] <- 0
  
  
  return(df)
}


df <- a81_inv(objkt,y1, y_out, y_ent1, gg$OB1_INV_MODE[i], gg$OB1_INV_SUMM[i], gg$OB1_INV_N[i], gg$OB1_INV_V[i], 
        gg$OB1_N[i], gg$OB1_V[i], gg$OB1_INV_K_BUDG[i], gg$OB1_R_CR[i], gg$OB1_T_CR[i])
df$ob1_debt_beg
df$ob1_paym_pers
df$ob1_paym
df$ob1_paym_debt
df$ob1_debt_end

