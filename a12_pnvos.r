# �12. ������ �����
a12_pnvos <- function(df,y1, y_out,  OB0_EHP_MODE,OB0_EHP_SUMM,Y_BASE){
  df$m_ehp1 <- 0
  df$m_ehp2 <- 0
  df$m_ehp3 <- 0
  df$m_ehp4 <- 0
  df$m_ehp5 <- 0
  df$p0_ehp5 <- 0
  df$p1_ehp5 <- 0
  df$c_ehp1 <- 0
  df$c_ehp2 <- 0
  df$c_ehp3 <- 0
  df$c_ehp4 <- 0
  df$c_ehp5 <- 0
  df$ehp <- 0
  if(OB0_EHP_MODE=="1"){
    i <- y1-year_start+1
    while(i<=y_out-year_start+1){
      df$m_ehp1[i] = df$m0_work[i]*df$P0EHP1[i] + df$m1_work[i]*df$P1EHP1[i]
      df$c_ehp1[i] = df$m_ehp1[i] * df$p_ehp1[i]/1000
      df$m_ehp2[i] = df$m0_work[i]*df$P0EHP2[i] + df$m1_work[i]*df$P1EHP2[i]
      df$c_ehp2[i] = df$m_ehp2[i] * df$p_ehp2[i]/1000
      df$m_ehp3[i] = df$m0_work[i]*df$P0EHP3[i] + df$m1_work[i]*df$P1EHP3[i]
      df$c_ehp3[i] = df$m_ehp3[i] * df$p_ehp3[i]/1000
      df$m_ehp4[i] = df$m0_work[i]*df$P0EHP4[i] + df$m1_work[i]*df$P1EHP4[i]
      df$c_ehp4[i] = df$m_ehp4[i] * df$p_ehp4[i]/1000
      df$p0_ehp5[i] = 1 - (df$P0EHP1[i]+df$P0EHP2[i]+df$P0EHP3[i]+df$P0EHP4[i])
      df$p1_ehp5[i] = 1 - (df$P1EHP1[i]+df$P1EHP2[i]+df$P1EHP3[i]+df$P1EHP4[i])
      df$m_ehp5[i] = df$m0_work[i]*df$p0_ehp5[i] + df$m1_work[i]*df$p1_ehp5[i]
      df$c_ehp5[i] = df$m_ehp5[i] * df$p_ehp5[i]/1000
      df$ehp[i] = df$c_ehp1[i] + df$c_ehp2[i] + df$c_ehp3[i] + df$c_ehp4[i] + df$c_ehp5[i]
      i <- i+1
    }
  }else{
    ehp_unit <- OB0_EHP_SUMM/(df$m0_work[Y_BASE-year_start+1]+df$m1_work[Y_BASE-year_start+1])
    i <- y1-year_start+1
    while(i<=y_out-year_start+1){
      df$ehp[i] <- (df$m0_work[i]+df$m1_work[i])*ehp_unit*df$k_consum_pr_base[i]
      
      i <- i+1
    }
    
    
    
  }
  return(df)
}
df <- objkt
