#
a0_base_ind <- function(df,Y_BASE,y_out){
  df$k_constr_pr_base <- 1
  df$k_consum_pr_base <-1
  df$k_elect_pr_base <- 1
  df$k_fuel_pr_base <- 1
  df$k_gas_pr_base <- 1
  df$k_constr_pr_base[Y_BASE-year_start+1] <- 1
  df$k_consum_pr_base[Y_BASE-year_start+1] <-1
  df$k_elect_pr_base[Y_BASE-year_start+1] <- 1
  df$k_fuel_pr_base[Y_BASE-year_start+1] <- 1
  df$k_gas_pr_base[Y_BASE-year_start+1]<-1
  
  i <- Y_BASE+1-year_start+1
  while(i<=y_out-year_start+1){
    df$k_constr_pr_base[i] <- df$k_constr_pr_base[i-1]*df$k_constr_pr_ch[i]
    df$k_consum_pr_base[i] <- df$k_consum_pr_base[i-1]*df$k_consum_pr_ch[i]
    df$k_elect_pr_base[i] <- df$k_elect_pr_base[i-1]*df$k_elect_pr_ch[i]
    df$k_fuel_pr_base[i] <- df$k_fuel_pr_base[i-1]*df$k_fuel_pr_ch[i]
    df$k_gas_pr_base[i] <- df$k_gas_pr_base[i-1]*df$k_gas_pr_ch[i]
    i <- i+1
  }
  return(df)
}
