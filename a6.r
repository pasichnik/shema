# �6. ������ ���������� ��������� � ����������� ��� �������� (�� ������� ������� 1)
a6_ost_price <- function(df,y1, y_out, y_ent1, OB1_D_ENTER, OB1_T_AM, OB1_INV_MODE, 
                         OB1_INV_SUMM, OB1_INV_N, OB1_INV_V, OB1_N, OB1_V){
  df$ob1_cost_enter <- 0
  df$ob1_cost_enter_ind <- 0
  df$ob1_cost_beg <- 0
  df$ob1_cost_end <- 0
  df$ob1_k <- 0
  df$ob1_taxprop_base <- 0
  df$ob1_am <- 0
  
  ob1_days_amm <-  as.numeric(format(as.Date(paste(y_ent1,12,31,sep = "/")),"%j"))
  ob1_days_pass <- as.numeric(format(OB1_D_ENTER,"%j")) -1
  ob1_k_dis <- (ob1_days_amm-ob1_days_pass)/ob1_days_amm
  
  ob1_cost <- ifelse(OB1_INV_MODE == "1", OB1_INV_SUMM, OB1_INV_N * OB1_N + OB1_INV_V * OB1_V )
  
  i <- y1-year_start+1
  
  while (i<=y_out-year_start+1) {
    if(i == y_ent1-year_start+1){
      df$ob1_cost_enter[i] <- ob1_cost
      df$ob1_cost_enter_ind[i] <- df$ob1_cost_enter[i]*df$k_constr_pr_base[i-1]
    }else{
      df$ob1_cost_enter[i] <- 0
      df$ob1_cost_enter_ind[i] <- 0
    }
    i <- i+1
  }
  ob1_am_dis <- ifelse(OB1_T_AM>0,df$ob1_cost_enter_ind[y_ent1-year_start+1]/OB1_T_AM,0)
  
  i <- y1-year_start+1
  while(i<=y_out-year_start+1){
    if(i == y1-year_start+1 | i == y_ent1-year_start+1){
      df$ob1_cost_beg[i] <- df$ob1_cost_enter_ind[i]
    }else{
      df$ob1_cost_beg[i] <- df$ob1_cost_end[i-1]
    }
    
    df$ob1_k[i] <- ifelse(i==y_ent1-year_start+1,ob1_k_dis,1)
    
    df$ob1_am[i] <- min(df$ob1_cost_beg[i],ob1_am_dis*df$ob1_k[i])
    df$ob1_cost_end[i] <- df$ob1_cost_beg[i]-df$ob1_am[i]
    df$ob1_taxprop_base[i] <- df$ob1_k[i]*(df$ob1_cost_beg[i]+df$ob1_cost_end[i])/2
    i <- i+1
  }
  
  return(df)
}

