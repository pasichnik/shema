# �9. ������ ������������ ��������

a9_rash <- function(df,y1, y_out, Y_BASE, OB_COND, y_ent,ie, OB0_OPEX_MODE, OB0_OPEX_SUMM, opex1_10, OB0_OPEX_UNIT){
  
  df$ob0_opex <- 0
  df$ob0_opex_enter <- 0
  df$ob0_opex_enter_ind <- 0
  
  ob0_y_enter <- ifelse(OB_COND == "1",y1,y_ent)
  
  ob0_opex_summ <- ifelse(OB0_OPEX_MODE==1,opex1_10,
                          ifelse(OB0_OPEX_MODE==2,OB0_OPEX_SUMM,
                                 OB0_OPEX_UNIT*(df$m0_work[Y_BASE-year_start+1]+df$m1_work[Y_BASE-year_start+1])))
  
  df$k_iie_base <- 1
  i <- Y_BASE-year_start+1
  while(i<=y_out-year_start+1){
    df$k_iie_base[i] <- df$k_iie_base[i-1]*(1-ie)
    i <- i+1
  }
  i <- y1-year_start+1
  while(i<=y_out-year_start+1){
    df$ob0_opex_enter[i] <- ifelse(i==ob0_y_enter-year_start+1,ob0_opex_summ,0)
    df$ob0_opex_enter_ind[i] <- df$ob0_opex_enter[i]*df$k_consum_pr_base[i]
    if(df$ob0_opex_enter[i]>0){
      df$ob0_opex[i] <- df$ob0_opex_enter_ind[i]
    }else{
      if(df$m0_work[i-1]+df$m1_work[i-1] > 0){
        df$ob0_opex[i] <- df$ob0_opex[i-1]*(1-ie)*df$k_constr_pr_ch[i]*(df$m0_work[i]+
                                      df$m1_work[i])/(df$m0_work[i-1]+df$m1_work[i-1])
        }else{
          df$ob0_opex[i] <- df$ob0_opex[Y_BASE-year_start+1]*df$k_consum_pr_base[i]*df$k_iie_base[i]*
            (df$m0_work[i]+df$m1_work[i])/(df$m0_work[Y_BASE-year_start+1] + df$m1_work[Y_BASE-year_start+1])
        
      }
    }
    df$ob0_opex[i] <- ifelse(is.na(df$ob0_opex[i]),0,df$ob0_opex[i])
    i <- i+1
  }
  
  return(df)
}

Y_BASE <- 2016
ie <- 0.01
