#�7.2. ������ ���������� ��� ������� (����� ����������)
a72_invest <- function(df,y1, y_out, y_ent, OB_COND, OB0_INV_MODE, OB0_INV_SUMM, OB0_INV_N, OB0_INV_V, 
                       OB0_N, OB0_V, OB0_INV_K_BUDG, OB0_T_CR,OB0_T_AM){
  df$ob0_inv_summ <- 0
  df$ob0_inv_summ_ind <- 0
  df$ob0_debt_beg <- 0
  df$ob0_paym <- 0
  df$ob0_debt_end <- 0
  
  if(OB_COND =="1") return(df)
  ob0_inv_summ_dis <- ifelse(OB0_INV_MODE ==1,OB0_INV_SUMM*(1- OB0_INV_K_BUDG),
                             (OB0_INV_N*OB0_N+OB0_INV_V*OB0_V)*(1-OB0_INV_K_BUDG))
  i <- y1-1-year_start+1
  while (i<=y_out-year_start+1) {
    if(i==y_ent-1-year_start+1){
      df$ob0_inv_summ[i] <- ob0_inv_summ_dis
      df$ob0_inv_summ_ind[i] <- df$ob0_inv_summ[i]*df$k_constr_pr_base[i]
    }else{
      df$ob0_inv_summ[i] <- 0
      df$ob0_inv_summ_ind[i] <- 0
    }
    i <- i+1
  }
  obo_paym_dis <- df$ob0_inv_summ_ind[y_ent-1-year_start+1]/OB0_T_AM

  df$ob0_debt_end[y1-1-year_start+1] <- df$ob0_inv_summ_ind[y1-1-year_start+1]
  i <- y1-year_start+1
  while(i<=y_out-year_start+1){
    df$ob0_debt_beg[i] <-ifelse(df$ob0_inv_summ[i]>0,df$ob0_inv_summ_ind[i],df$ob0_debt_end[i-1])
    df$ob0_paym[i] <- min(df$ob0_debt_beg[i],df$ob0_paym_dis)
    df$ob0_debt_end[i] <- df$ob0_debt_beg[i]-df$ob0_paym[i]
    
   i <- i+1  
  }
  return(df)
}
