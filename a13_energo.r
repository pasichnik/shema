# �13. ������ ������ �� �������������
a13_energo <- function(df, y1, y_out, Y_BASE, OB0_ENR_MODE, OB0_ENR_SUMM, OB0_ENR_ELECT, OB0_ENR_FUEL, OB0_ENR_GAS, OB0_ENR_UNIT){
  
  df$enr <- 0
  
  
  if(OB0_ENR_MODE=="1"){
    if((df$m0_work[Y_BASE-year_start+1]+df$m1_work[Y_BASE-year_start+1])>0){
      enr_elect_unit <- OB0_ENR_ELECT/(df$m0_work[Y_BASE-year_start+1]+df$m1_work[Y_BASE-year_start+1])
      enr_fuel_unit <- OB0_ENR_FUEL/(df$m0_work[Y_BASE-year_start+1]+df$m1_work[Y_BASE-year_start+1])
      enr_gas_unit <-  OB0_ENR_GAS/(df$m0_work[Y_BASE-year_start+1]+df$m1_work[Y_BASE-year_start+1]) 
    } else{
      enr_elect_unit <- 0
      enr_fuel_unit <- 0
      enr_gas_unit <- 0
    }
    
    
    i <- y1-year_start+1
    while(i<=y_out-year_start+1){
      df$enr[i] <- (df$m0_work[i]+df$m1_work[i])*(enr_elect_unit*df$k_elect_pr_base[i]+
                                                  enr_fuel_unit*df$k_fuel_pr_base[i]+
                                                  enr_gas_unit*df$k_gas_pr_base[i])
      i <- i+1
    }
  }else{
    if(OB0_ENR_MODE =="2"){
      enr_unit <- OB0_ENR_SUMM/(df$m0_work[Y_BASE-year_start+1]+df$m1_work[Y_BASE-year_start+1])
      i <- y1-year_start+1
      while(i<=y_out-year_start+1){
        df$enr[i] <- (df$m0_work[i]+df$m1_work[i])*enr_unit*df$k_consum_pr_base[i]
        i <- i+1
      }
    }else{
      i <- y1-year_start+1
      while(i<=y_out-year_start){
        df$enr[i] <- (df$m0_work[i]+df$m1_work[i])*OB0_ENR_UNIT*df$k_consum_pr_base[i]/1000
        
        
        i <- i+1
      }
    }
  }
  return(df) 
}

